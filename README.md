# Password module

Simple python module to generate password using passlib library

## Usage

### Install requirements
```
pip install -r requirements.txt
```
```python

>>> from pypassword import Password
>>> pw = Password(20)
>>> pw.getpass
{'plain_text': '*1U9hn,IvTR4Gde*by1A', 'md5': 'c01cb2c7b7af662b3847e844c85cd730', 'sha256': '54be8dad97b08a8e4965119c97bd4ca3d21caa0b59a9a318429e8669f2d3c623'}
>>> pw.plain_text
'*1U9hn,IvTR4Gde*by1A'
>>> pw.md5
'c01cb2c7b7af662b3847e844c85cd730'
>>> pw.sha256
'54be8dad97b08a8e4965119c97bd4ca3d21caa0b59a9a318429e8669f2d3c623'
>>> pw.sha512_crypt
'$6$rounds=656000$FfWsWyv9/0X.Glez$Sr3HM54nZJcr.H..JVEsuL7DvNIWrQjBKqhMc7V8h/MP94lElCWPVrt4HqDK7kjaZF96VntwuaI1vPpyliMJC/'
```

## Running tests

> :warning: `verbose` option is used in this example, but not required

```bash
bdenece@ubuntu-server:~/dev/ws-python-library$ python -m unittest tests/test_password.py --verbose
test_check_var_type (tests.test_password.TestPassword.test_check_var_type)
Return true if type are correct ... ok
test_custom_lenght (tests.test_password.TestPassword.test_custom_lenght)
Return true if password lenght = 30 ... ok
test_default_car (tests.test_password.TestPassword.test_default_car)
Return true if password lenght = 16 ... ok
test_get_md5 (tests.test_password.TestPassword.test_get_md5)
Return true if getpass_md5() is matching regex '^[0-9a-fA-F]{32}$' ... ok
test_get_sha256 (tests.test_password.TestPassword.test_get_sha256)
Return true if getpass_sha256() is matching regex '^[A-Fa-f0-9]{64}$' ... ok
test_get_sha512_crypt (tests.test_password.TestPassword.test_get_sha512_crypt)
Return true if getpass_sha512_crypt() is correctly encrypted ... ok

----------------------------------------------------------------------
Ran 6 tests in 3.580s

OK
```