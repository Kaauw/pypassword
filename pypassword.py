"""Simple python module to generate password using passlib library."""
from dataclasses import dataclass
import secrets
import hashlib
from passlib.hash import sha512_crypt


@dataclass
class Password:
    """Password Class.

    Attributes
    ----------
        lenght (int, optional): Password lenght. Defaults to 16.

    """

    lenght: int = 16

    def __post_init__(self):
        """Generate password and encrypt them after class initialization."""
        self.__password = self.__generate()
        self.__encrypt()

    def __generate(self) -> str:
        """Password generation algorithm.

        Returns:
            str: generated password string
        """
        password = []
        lower_char = (
                'a', 'b', 'c', 'd',
                'e', 'f', 'g', 'h',
                'i', 'j', 'k', 'm',
                'n', 'o', 'q', 'r',
                's', 't', 'u', 'v',
                'w', 'x', 'y', 'z'
                )
        upper_char = (
                'A', 'B', 'C', 'D',
                'E', 'F', 'G', 'H',
                'I', 'J', 'K', 'L',
                'M', 'N', 'Q', 'R',
                'S', 'T', 'U', 'V',
                'W', 'X', 'Y', 'Z'
                )
        numbers = (
                '1', '2',
                '3', '4',
                '5', '6',
                '7', '8',
                '9', '0'
                )
        specials = ('@', '&', ';',
                    ',', '#', '*')

        for _ in list(range(self.lenght)):
            rand_char = secrets.choice(list(range(1, 5)))
            match rand_char:
                case 1:
                    password.append(secrets.choice(lower_char))
                case 2:
                    password.append(secrets.choice(upper_char))
                case 3:
                    password.append(secrets.choice(numbers))
                case 4:
                    password.append(secrets.choice(specials))

        return "".join(password)

    def __encrypt(self):
        """Encrypt password to md5, sha256 & sha512."""
        self.__md5 = hashlib.md5(self.__password.encode()).hexdigest()
        self.__sha256 = hashlib.sha256(self.__password.encode()).hexdigest()
        self.__sha512_crypt = sha512_crypt.hash(self.__password.encode())

    @property
    def getpass(self) -> dict:
        """Return a dict with password as plain text, md5 and sha256 hashes.

        Returns:
            dict: {
                'plain_text': value,
                'md5': value;
                'sha256': value,
                'sha512_crypt': value
                }
        """
        return {
            'plain_text': self.__password,
            'md5': self.__md5,
            'sha256': self.__sha256,
            'sha512_crypt': self.__sha512_crypt
            }

    @property
    def plain_text(self) -> str:
        """Return a string with plain text password.

        Returns:
            str: Plain text password
        """
        return self.__password

    @property
    def md5(self) -> str:
        """Return a string with md5 hashed password.

        Returns:
            str: md5 hashed password
        """
        return self.__md5

    @property
    def sha256(self) -> str:
        """Return a string with sha256 hashed password.

        Returns:
            str: sha256 hashed password
        """
        return self.__sha256

    @property
    def sha512_crypt(self) -> str:
        """Return a string with sha512 crypt ($6$) encrypted password.

        Returns:
            str: sha512 encrypted password
        """
        return self.__sha512_crypt


if __name__ == "__main__":
    print(Password.__doc__)
